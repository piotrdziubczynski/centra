<?php

declare(strict_types=1);

define('ROOT_DIR', dirname(__DIR__));

use DI\Container;
use FastRoute\Dispatcher;
use Symfony\Component\HttpFoundation\Request;

/** @var Container $container */
$container = require_once ROOT_DIR . '/kernel/bootstrap.php';
$request = $container->get(Request::class);
/** @var Dispatcher $routes */
$routes = require_once ROOT_DIR . '/kernel/routes.php';
$route = $routes->dispatch($request->server->get('REQUEST_METHOD'), getUri($request->server->get('REQUEST_URI')));

switch ($route[0]) {
    case Dispatcher::FOUND:
        $container->call($route[1], $route[2]);
        break;
    case Dispatcher::METHOD_NOT_ALLOWED:
        $message = sprintf('%s Method Not Allowed', 405);
        header(sprintf('Access-Control-Allow-Methods: %s', implode(', ', $route[1])), true, 405);
        die($message);
        break;
    case Dispatcher::NOT_FOUND:
    default:
        $message = sprintf('%s Not Found', 404);
        header(sprintf('%s %s', $request->server->get('SERVER_PROTOCOL'), $message), true, 404);
        die($message);
}
