<?php

declare(strict_types=1);

namespace App\Adapter\Github;

use Github\Api\Issue;
use Github\Client;

/**
 * Class Github
 *
 * @package App\Adapter\Github
 */
final class Github implements GithubInterface
{
    private Client $client;
    private string $account;

    /**
     * Github constructor.
     *
     * @param string $token
     * @param string $account
     */
    public function __construct(string $token, string $account)
    {
        $this->account = $account;
        $this->client = new Client();
        $this->client->authenticate($token, null, Client::AUTH_HTTP_TOKEN);
    }

    /**
     * @inheritDoc
     */
    public function getMilestones(string $repository): array
    {
        /** @var Issue $issue */
        $issue = $this->client->api('issue');
        $milestones = $issue->milestones();

        return $milestones->all(
            $this->account,
            $repository,
            [
                'state' => 'all',
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function getIssues(string $repository): array
    {
        /** @var Issue $issue */
        $issue = $this->client->api('issue');

        return $issue->all(
            $this->account,
            $repository,
            [
                'state' => 'all',
            ]
        );
    }
}
