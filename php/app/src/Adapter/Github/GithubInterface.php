<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * Filename: GithubInterface.php
 * User: piotr
 * Date: 08.06.2020
 * Time: 20:01
 */

namespace App\Adapter\Github;

/**
 * Interface GithubInterface
 *
 * @package App\Adapter\Github
 */
interface GithubInterface
{
    /**
     * @link https://developer.github.com/v3/issues/milestones/#list-milestones
     *
     * @param string $repository
     *
     * @return array
     */
    public function getMilestones(string $repository): array;

    /**
     * @link https://developer.github.com/v3/issues/#list-repository-issues
     *
     * @param string $repository
     *
     * @return array
     */
    public function getIssues(string $repository): array;
}
