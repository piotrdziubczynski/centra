<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * Filename: Fetch.php
 * User: piotr
 * Date: 07.06.2020
 * Time: 19:46
 */

namespace App\Service\Fetch;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;

/**
 * Class Fetch
 *
 * @package App\Service\Fetch
 */
final class Fetch implements FetchInterface
{
    private Client $client;
    private ?ResponseInterface $response;

    /**
     * Fetch constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
        $this->response = null;
    }

    /**
     * @inheritDoc
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @inheritDoc
     */
    public function getResponse(): string
    {
        if ($this->response === null) {
            throw new RuntimeException('Before using this method, make the request first.');
        }

        return $this->response->getBody()->getContents();
    }

    /**
     * @inheritDoc
     *
     * @throws GuzzleException
     */
    public function get(string $uri, array $options = []): void
    {
        $this->response = $this->client->request('GET', $uri, $options);
    }

    /**
     * @inheritDoc
     *
     * @throws GuzzleException
     */
    public function post(string $uri, array $options = []): void
    {
        $this->response = $this->client->request('POST', $uri, $options);
    }
}
