<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * Filename: FetchInterface.php
 * User: piotr
 * Date: 07.06.2020
 * Time: 19:39
 */

namespace App\Service\Fetch;

use GuzzleHttp\Client;

/**
 * Interface FetchInterface
 *
 * @package App\Service\Fetch
 */
interface FetchInterface
{
    /**
     * @return Client
     */
    public function getClient(): Client;

    /**
     * @return string
     */
    public function getResponse(): string;

    /**
     * @param string $uri
     * @param array $options
     */
    public function get(string $uri, array $options = []): void;

    /**
     * @param string $uri
     * @param array $options
     */
    public function post(string $uri, array $options = []): void;
}
