<?php

declare(strict_types=1);

namespace App\Service\KanbanBoard;

use App\Service\Fetch\FetchInterface;
use RuntimeException;

/**
 * Class Authentication
 *
 * @package App\Service\KanbanBoard
 */
final class Authentication
{
    private ?string $id;
    private ?string $secret;
    private FetchInterface $fetch;
    private ?string $state;

    /**
     * Authentication constructor.
     *
     * @param FetchInterface $fetch
     */
    public function __construct(FetchInterface $fetch)
    {
        $this->id = null;
        $this->secret = null;
        $this->fetch = $fetch;
        $this->state = null;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function buildQuery(array $data): string
    {
        return sprintf('?%s', http_build_query($data));
    }

    /**
     * @param string|null $id
     */
    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    /**
     * @param string|null $secret
     */
    public function setSecret(?string $secret): void
    {
        $this->secret = $secret;
    }

    /**
     * @param string|null $state
     */
    public function setState(?string $state): void
    {
        $this->state = $state;
    }

    /**
     * @param string $redirectUri
     */
    public function authorize(string $redirectUri): void
    {
        if ($this->id === null || $this->state === null) {
            throw new RuntimeException('Before using this method, set ID and State first.');
        }

        $url = sprintf(
            'https://github.com/login/oauth/authorize%s',
            $this->buildQuery(
                [
                    'client_id' => $this->id,
                    'scope' => 'repo',
                    'state' => $this->state,
                    'redirect_uri' => $redirectUri,
                ]
            )
        );

        header(sprintf('Location: %s', $url), true, 302);
        exit();
    }

    /**
     * @param string $code
     *
     * @return string|null
     */
    public function getToken(string $code): ?string
    {
        $url = sprintf(
            'https://github.com/login/oauth/access_token%s',
            $this->buildQuery(
                [
                    'client_id' => $this->id,
                    'client_secret' => $this->secret,
                    'code' => $code,
                    'state' => $this->state,
                ]
            )
        );

        $this->fetch->post($url);
        parse_str($this->fetch->getResponse(), $response);

//        return $response['access_token'] ?? $response['error_description'];
        return $response['access_token'] ?? null;
    }
}
