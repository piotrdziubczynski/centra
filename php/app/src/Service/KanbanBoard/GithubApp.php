<?php

declare(strict_types=1);

namespace App\Service\KanbanBoard;

use App\Adapter\Github\GithubInterface;
use Michelf\Markdown;

/**
 * Class Application
 *
 * @package App\Service\KanbanBoard
 */
final class GithubApp
{
    private GithubInterface $github;
    private ?array $milestonesWithIssues;
    private array $pausedLabels;
    private string $repository;

    /**
     * GithubApp constructor.
     *
     * @param GithubInterface $github
     * @param string $repository
     * @param array $pausedLabels
     */
    public function __construct(GithubInterface $github, string $repository, array $pausedLabels = [])
    {
        $this->github = $github;
        $this->milestonesWithIssues = null;
        $this->pausedLabels = $pausedLabels;
        $this->repository = $repository;
    }

    /**
     * @param array $issues
     *
     * @return array
     */
    private function createIssuesData(array $issues): array
    {
        $result = [];

        foreach ($issues as $key => $issue) {
            $result[self::_state($issue)][] = [
                'id' => $issue['id'],
                'number' => $issue['number'],
                'title' => $issue['title'],
                'body' => Markdown::defaultTransform($issue['body']),
                'url' => $issue['html_url'],
                'assignee' => ($issue['assignee'] !== null) ? $issue['assignee']['avatar_url'] . '&s=16' : null,
                'paused' => self::_labels_match($issue, $this->pausedLabels),
                'progress' => self::_percent(
                    substr_count(strtolower($issue['body']), '[x]'),
                    substr_count(strtolower($issue['body']), '[ ]')
                ),
                'closed' => $issue['closed_at'],
            ];
        }

        $active = $result['active'] ?? [];

        usort(
            $active,
            function ($a, $b) {
                return count($a['paused']) - count($b['paused']) === 0 ?
                    strcmp($a['title'], $b['title']) :
                    count($a['paused']) - count($b['paused']);
            }
        );

        return $result;
    }

    /**
     * @param array $milestones
     *
     * @return array
     */
    private function createMilestonesData(array $milestones): array
    {
        $result = [];

        foreach ($milestones as $key => $milestone) {
            $issues = $this->createIssuesData($milestone['issues']);

            $result[] = [
                'milestone' => $milestone['title'],
                'url' => $milestone['html_url'],
                'progress' => self::_percent($milestone['closed_issues'], $milestone['open_issues']),
                'queued' => $issues['queued'] ?? [],
                'active' => $issues['active'] ?? [],
                'completed' => $issues['completed'] ?? [],
            ];
        }

        return $result;
    }

    public function fetchMilestonesWithIssues(): void
    {
        $milestones = [];
        $issues = $this->github->getIssues($this->repository);

        foreach ($issues as $key => $issue) {
            if (!empty($issue['pull_request'] ?? []) || $issue['milestone'] === null) {
                continue;
            }

            $milestone = $issue['milestone'];
            unset($issue['milestone']);

            if (empty($milestones[$milestone['title']] ?? [])) {
                $milestones[$milestone['title']] = $milestone;
            }

            $milestones[$milestone['title']]['issues'][] = $issue;
        }

        unset($issues, $issue, $milestone);
        ksort($milestones);

        $this->milestonesWithIssues = array_values($milestones);
    }

    /**
     * @return array
     */
    public function createBoard(): array
    {
        if ($this->milestonesWithIssues === null) {
            return [];
        }

        return $this->createMilestonesData($this->milestonesWithIssues);
    }

    /**
     * @param $issue
     *
     * @return string
     */
    private static function _state(array $issue): string
    {
        $state = 'queued';

        if ($issue['assignee'] !== null) {
            $state = 'active';
        }

        if ($issue['state'] === 'closed') {
            $state = 'completed';
        }

        return $state;
    }

    /**
     * @param array $issue
     * @param array $pausedLabels
     *
     * @return array
     */
    private static function _labels_match(array $issue, array $pausedLabels): array
    {
        foreach ($issue['labels'] as $label) {
            if (in_array($label['name'], $pausedLabels, true)) {
                return [$label['name']];
            }
        }

        return [];
    }

    /**
     * @param int $complete
     * @param int $remaining
     *
     * @return array
     */
    private static function _percent(int $complete, int $remaining): array
    {
        if ($complete < 0) {
            $complete = 0;
        }

        if ($remaining < 0) {
            $remaining = 0;
        }

        $total = $complete + $remaining;
        $percent = 0;

        if ($total > 0) {
            $percent = (int)(round($complete / $total * 100) ?: 0);
        }

        return [
            'total' => $total,
            'complete' => $complete,
            'remaining' => $remaining,
            'percent' => $percent,
        ];
    }
}
