<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * Filename: AbstractController.php
 * User: piotr
 * Date: 05.06.2020
 * Time: 23:50
 */

namespace App\Controller;

use DI\Container;
use Mustache_Engine;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Throwable;

/**
 * Class AbstractController
 *
 * @package App\Controller
 */
abstract class AbstractController
{
    private Container $container;
    private Mustache_Engine $mustache;
    private SessionInterface $session;

    /**
     * AbstractController constructor.
     *
     * @param Container $container
     * @param Mustache_Engine $mustache
     * @param Request $request
     */
    public function __construct(Container $container, Mustache_Engine $mustache, Request $request)
    {
        $this->container = $container;
        $this->mustache = $mustache;
        $this->session = $request->getSession();
    }

    /**
     * @param FlashBagInterface $flashBag
     *
     * @return array
     */
    private function getAllFlashes(FlashBagInterface $flashBag): array
    {
        $flashes = [];

        foreach ($flashBag->all() as $type => $messages) {
            $flashes[] = [
                'type' => $type,
                'messages' => $messages,
            ];
        }

        return $flashes;
    }

    /**
     * @param string $name
     *
     * @return mixed|null
     */
    protected function getDefinition(string $name)
    {
        try {
            return $this->container->get($name);
        } catch (Throwable $e) {
            return null;
        }
    }

    /**
     * @param string $type
     * @param string $message
     */
    protected function addFlash(string $type, string $message): void
    {
        /** @var FlashBagInterface $flashes */
        $flashes = $this->session->getBag('flashes');
        $flashes->add($type, $message);
    }

    /**
     * @param string $template
     * @param array $data
     * @param int $status
     *
     * @return Response
     */
    protected function render(string $template, array $data = [], int $status = Response::HTTP_OK): Response
    {
        /** @var FlashBagInterface $flashBag */
        $flashBag = $this->session->getBag('flashes');
        $response = new Response(
            $this->mustache->render(
                $template,
                array_replace(
                    [
                        'title' => 'New Page',
                        'flashes' => $this->getAllFlashes($flashBag),
                    ],
                    $data
                )
            ),
            $status
        );

        return $response->send();
    }
}
