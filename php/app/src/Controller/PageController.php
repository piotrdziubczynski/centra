<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * Filename: PageController.php
 * User: piotr
 * Date: 05.06.2020
 * Time: 21:16
 */

namespace App\Controller;

use App\Adapter\Github\Github;
use App\Service\KanbanBoard\GithubApp;
use App\Service\KanbanBoard\Authentication;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PageController
 *
 * @package App\Controller
 */
final class PageController extends AbstractController
{
    /**
     * @param Request $request
     * @param Authentication $auth
     *
     * @return Response
     */
    public function boardAction(Request $request, Authentication $auth): Response
    {
        $repositories = $this->getDefinition('gh.repositories') ?? '';
        $repositories = explode('|', $repositories);

        return $this->render(
            'index',
            [
                'title' => 'Kanban Board :: Repositories',
                'repositories' => $repositories,
            ]
        );
    }

    /**
     * @param Request $request
     * @param Authentication $auth
     * @param string $repository
     *
     * @return Response
     */
    public function boardRepositoryAction(Request $request, Authentication $auth, string $repository): Response
    {
        $repositories = $this->getDefinition('gh.repositories') ?? '';
        $repositories = explode('|', $repositories);

        if (!in_array($repository, $repositories, true)) {
            $this->addFlash('warning', 'A selected repository is not allowed.');
            // todo: redirect to list...
        }

        $token = $request->getSession()->get('gh.token');

        if ($token === null) {
            if ($request->query->get('code') === null) {
                $auth->authorize($request->getUri());
            }

            $token = $auth->getToken($request->query->get('code'));

            if ($token === null) {
                $this->addFlash('danger', 'Failed to get token. Please contact support.');
            }

            $request->getSession()->set('gh.token', $token);
        }

        $milestones = [];

        if ($token !== null) {
            $app = new GithubApp(
                new Github($token, $this->getDefinition('gh.account')),
                $repository,
                ['waiting for feedback']);
            $app->fetchMilestonesWithIssues();

            $milestones = $app->createBoard();
        }

        return $this->render(
            'board/repository',
            [
                'title' => 'Kanban Board :: Repository: ' . $repository,
                'milestones' => $milestones,
            ]
        );
    }
}
