<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * Filename: functions.php
 * User: piotr
 * Date: 05.06.2020
 * Time: 23:28
 */

/**
 * @param mixed ...$args
 */
function dump(...$args)
{
    foreach ($args as $key => $arg) {
        echo '<pre>';
        var_dump($arg);
        echo '</pre>';
    }
}

/**
 * @param mixed ...$args
 */
function dd(...$args)
{
    dump(...$args);
    die();
}

/**
 * @param string $uri
 *
 * @return string
 */
function getUri(string $uri): string
{
    $pos = strpos($uri, '?');

    if ($pos !== false) {
        $uri = substr($uri, 0, $pos);
    }

    return rawurldecode($uri);
}
