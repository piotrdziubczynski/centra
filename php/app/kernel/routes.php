<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * Filename: routes.php
 * User: piotr
 * Date: 05.06.2020
 * Time: 21:16
 */

use App\Controller\PageController;
use FastRoute\RouteCollector;

use function FastRoute\simpleDispatcher;

return simpleDispatcher(
    function (RouteCollector $route) {
        $route->addRoute(['GET'], '/[board[/]]', [PageController::class, 'boardAction']);
        $route->addRoute(['GET'], '/board/{repository:[a-z0-9-]+}[/]', [PageController::class, 'boardRepositoryAction']);
    }
);
