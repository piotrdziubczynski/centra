<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * Filename: autowire.php
 * User: piotr
 * Date: 05.06.2020
 * Time: 21:04
 */

use App\Service\Fetch\Fetch;
use App\Service\Fetch\FetchInterface;
use App\Service\KanbanBoard\Authentication;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

use function DI\create;
use function DI\env;
use function DI\factory;
use function DI\get;

return [
    'app.env' => env('APP_ENV', 'prod'),
    'app.secret' => env('APP_SECRET'),

    'gh.client.id' => env('GH_CLIENT_ID'),
    'gh.client.secret' => env('GH_CLIENT_SECRET'),
    'gh.account' => env('GH_ACCOUNT'),
    'gh.repositories' => env('GH_REPOSITORIES'),

    FetchInterface::class => get(Fetch::class),
    Authentication::class => create()
        ->constructor(get(FetchInterface::class))
        ->method('setId', get('gh.client.id'))
        ->method('setSecret', get('gh.client.secret'))
        ->method('setState', get('app.secret')),
    Mustache_Engine::class => factory(
        function () {
            return new Mustache_Engine(
                [
                    'loader' => new Mustache_Loader_FilesystemLoader(ROOT_DIR . '/views'),
                    'strict_callables' => true,
                ]
            );
        }
    ),
    Request::class => factory(
        function () {
            $session = new Session();
            $session->start();

            $request = Request::createFromGlobals();
            $request->setSession($session);

            return $request;
        }
    ),
];
