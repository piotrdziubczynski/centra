<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * Filename: bootstrap.php
 * User: piotr
 * Date: 03.06.2020
 * Time: 19:20
 */

require_once ROOT_DIR . '/vendor/autoload.php';
require_once __DIR__ . '/functions.php';

use DI\ContainerBuilder;
use Dotenv\Dotenv;

$dotenv = Dotenv::createMutable(ROOT_DIR);

try {
    $dotenv->load();

    $dotenv->required('APP_ENV');
    $dotenv->required('APP_SECRET')
        ->notEmpty();

    $dotenv->required('GH_CLIENT_ID')
        ->notEmpty();
    $dotenv->required('GH_CLIENT_SECRET')
        ->notEmpty();
    $dotenv->required('GH_ACCOUNT')
        ->notEmpty();
    $dotenv->required('GH_REPOSITORIES')
        ->notEmpty();
} catch (Throwable $e) {
    die($e->getMessage());
}

$builder = new ContainerBuilder();
$builder->addDefinitions(__DIR__ . '/autowire.php');

return $builder->build();
