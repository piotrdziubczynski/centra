# centra recruitment

Live demo: https://centra.dziubczynski.pl/

Readme: [link](https://gitlab.com/piotrdziubczynski/centra/-/blob/master/php/app/README.md)

## Installation

* Clone repository to your `projects` directory.
* Go to `<project_name>/php/app` and remove `.dist` extension from `.env` file.
* Edit `<project_name>/php/app/.env` file and fill the below values. I recommend using this [generator](https://www.random.org/strings/)
for `APP_SECRET` env variable. 16 chars long are OK.
```
APP_SECRET=
GH_CLIENT_ID=
GH_CLIENT_SECRET=
GH_ACCOUNT=
GH_REPOSITORIES=
```
* Go to `<project_name>/docker/logs/nginx` and remove `.dist` extension from both files.
* Now, you can run the app. Open terminal, go to the project directory and use below commands:
```
docker-compose up -d --build
```
```
docker exec -it <fpm_container_name> sh
> composer install
> exit
```
OK. That's it! Open your browser on `localhost` page and start viewing Kanban Board.